require 'logger'

require 'enforcr/version'

module Enforcr
  class << self
    attr_accessor :logger
  end
end

Enforcr.logger = Logger.new(STDERR)