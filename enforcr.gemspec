# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'enforcr/version'

Gem::Specification.new do |spec|
  spec.name = 'enforcr'
  spec.version = enforcr::VERSION
  spec.authors = %w(Anthony\ Kosednar)
  spec.email = %w(anthony@lockerhub.com)
  spec.summary = 'Judge network traffic for threats and take action.'
  spec.description = 'Ruby Base Security Node Software'
  spec.homepage = 'https://enforcr.com'

  spec.files = `git ls-files -z`.split("\x0")
  spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = %w(lib)
  spec.extra_rdoc_files = %w(README.md)

  spec.required_ruby_version = Gem::Requirement.new('>= 2.1.0')

  spec.add_development_dependency('bundler', '~> 1.7')
  spec.add_development_dependency('rake', '~> 10.0')
  spec.add_development_dependency 'rspec', '~> 3.1', '>= 3.1.0'

  spec.add_development_dependency('awesome_print', '~> 1.2', '>= 1.2.0')
  #spec.add_development_dependency('geminabox', '~> 0.12.4', '>= 0.12.4')
end